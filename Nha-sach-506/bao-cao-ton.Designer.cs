﻿namespace Nha_sach_506
{
    partial class frmBaoCaoTon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label2 = new System.Windows.Forms.Label();
            this.btnThoat_bct = new System.Windows.Forms.Button();
            this.txtNam_bct = new System.Windows.Forms.TextBox();
            this.lblNam_bct = new System.Windows.Forms.Label();
            this.txtThang_bct = new System.Windows.Forms.TextBox();
            this.lblThang_bct = new System.Windows.Forms.Label();
            this.dgvBaoCaoTon = new System.Windows.Forms.DataGridView();
            this.Label1 = new System.Windows.Forms.Label();
            this.picBaoCaoTon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBaoCaoTon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBaoCaoTon)).BeginInit();
            this.SuspendLayout();
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.Color.Black;
            this.Label2.Location = new System.Drawing.Point(374, 445);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(221, 15);
            this.Label2.TabIndex = 32;
            this.Label2.Text = "© Copyright 2016. 506 HUTECH Group";
            // 
            // btnThoat_bct
            // 
            this.btnThoat_bct.BackColor = System.Drawing.Color.Red;
            this.btnThoat_bct.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThoat_bct.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnThoat_bct.Location = new System.Drawing.Point(451, 182);
            this.btnThoat_bct.Name = "btnThoat_bct";
            this.btnThoat_bct.Size = new System.Drawing.Size(144, 46);
            this.btnThoat_bct.TabIndex = 31;
            this.btnThoat_bct.Text = "Thoát";
            this.btnThoat_bct.UseVisualStyleBackColor = false;
            // 
            // txtNam_bct
            // 
            this.txtNam_bct.Location = new System.Drawing.Point(234, 208);
            this.txtNam_bct.Name = "txtNam_bct";
            this.txtNam_bct.Size = new System.Drawing.Size(106, 20);
            this.txtNam_bct.TabIndex = 30;
            // 
            // lblNam_bct
            // 
            this.lblNam_bct.AutoSize = true;
            this.lblNam_bct.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNam_bct.Location = new System.Drawing.Point(187, 207);
            this.lblNam_bct.Name = "lblNam_bct";
            this.lblNam_bct.Size = new System.Drawing.Size(41, 18);
            this.lblNam_bct.TabIndex = 29;
            this.lblNam_bct.Text = "Năm";
            // 
            // txtThang_bct
            // 
            this.txtThang_bct.Location = new System.Drawing.Point(63, 208);
            this.txtThang_bct.Name = "txtThang_bct";
            this.txtThang_bct.Size = new System.Drawing.Size(105, 20);
            this.txtThang_bct.TabIndex = 28;
            // 
            // lblThang_bct
            // 
            this.lblThang_bct.AutoSize = true;
            this.lblThang_bct.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThang_bct.Location = new System.Drawing.Point(6, 207);
            this.lblThang_bct.Name = "lblThang_bct";
            this.lblThang_bct.Size = new System.Drawing.Size(51, 18);
            this.lblThang_bct.TabIndex = 27;
            this.lblThang_bct.Text = "Tháng";
            // 
            // dgvBaoCaoTon
            // 
            this.dgvBaoCaoTon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBaoCaoTon.Location = new System.Drawing.Point(11, 241);
            this.dgvBaoCaoTon.Name = "dgvBaoCaoTon";
            this.dgvBaoCaoTon.Size = new System.Drawing.Size(584, 198);
            this.dgvBaoCaoTon.TabIndex = 26;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Label1.Location = new System.Drawing.Point(227, 125);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(240, 37);
            this.Label1.TabIndex = 24;
            this.Label1.Text = "BÁO CÁO TỒN";
            // 
            // picBaoCaoTon
            // 
            this.picBaoCaoTon.Image = global::Nha_sach_506.Properties.Resources._506_LOGO;
            this.picBaoCaoTon.Location = new System.Drawing.Point(82, 12);
            this.picBaoCaoTon.Name = "picBaoCaoTon";
            this.picBaoCaoTon.Size = new System.Drawing.Size(412, 155);
            this.picBaoCaoTon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBaoCaoTon.TabIndex = 25;
            this.picBaoCaoTon.TabStop = false;
            // 
            // frmBaoCaoTon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 465);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.btnThoat_bct);
            this.Controls.Add(this.txtNam_bct);
            this.Controls.Add(this.lblNam_bct);
            this.Controls.Add(this.txtThang_bct);
            this.Controls.Add(this.lblThang_bct);
            this.Controls.Add(this.dgvBaoCaoTon);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.picBaoCaoTon);
            this.Name = "frmBaoCaoTon";
            this.Text = "Báo cáo tồn";
            ((System.ComponentModel.ISupportInitialize)(this.dgvBaoCaoTon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBaoCaoTon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Button btnThoat_bct;
        internal System.Windows.Forms.TextBox txtNam_bct;
        internal System.Windows.Forms.Label lblNam_bct;
        internal System.Windows.Forms.TextBox txtThang_bct;
        internal System.Windows.Forms.Label lblThang_bct;
        internal System.Windows.Forms.DataGridView dgvBaoCaoTon;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.PictureBox picBaoCaoTon;
    }
}