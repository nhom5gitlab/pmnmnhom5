﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Nha_sach_506
{
    public partial class frmPhieuThuTien : Form
    {
        public frmPhieuThuTien()
        {
            InitializeComponent();
        }

        private void frmPhieuThuTien_Load(object sender, EventArgs e)
        {
            lblDate_ptt.Text = DateTime.Now.ToShortDateString();
        }
    }
}
