﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace Nha_sach_506
{
    class KetnoiSQL
    {

        

            //----Khai báo, hàm mở kết nối
            public SqlConnection kn = new SqlConnection();
            public void kn_csdl()
            {
                //----Khi chạy trên máy khác phải đổi chuỗi kết nối này...
                string chuoikn = "Data Source=HV-83E078BEA2CA;Integrated Security=True";

                kn.ConnectionString = chuoikn;
                kn.Open();
            }
            //---Hàm dùng để lấy giá trị: tính tiền,......
            public string lay1giatri(string sql)
            {
                string kq = "";
                try
                {
                    kn_csdl();

                    SqlCommand sqlComm = new SqlCommand(sql, kn);
                    SqlDataReader r = sqlComm.ExecuteReader();
                    if (r.Read())
                    {
                        kq = r["tong"].ToString();
                    }
                }
                catch
                { }
                return kq;
            }

            //----Đóng kết nối
            public void dongketnoi()
            {
                if (kn.State == ConnectionState.Open)
                { kn.Close(); }
            }
            public DataTable bangdulieu = new DataTable();
            public DataTable laybang(string caulenh)
            {
                try
                {
                    kn_csdl();
                    SqlDataAdapter Adapter = new SqlDataAdapter(caulenh, kn);
                    DataSet ds = new DataSet();

                    Adapter.Fill(bangdulieu);
                }
                catch (System.Exception)
                {
                    bangdulieu = null;
                }
                finally
                {
                    dongketnoi();
                }

                return bangdulieu;
            }
            //----Hàm xử lí dùng cho nhap, xóa, sửa dữ liêu...
            public int xulydulieu(string caulenhsql)
            {
                int kq = 0;
                try
                {
                    kn_csdl();
                    SqlCommand lenh = new SqlCommand(caulenhsql, kn);
                    kq = lenh.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //Thông báo lỗi ra!
                    MessageBox.Show(ex.Message);

                    kq = 0;
                }
                finally
                {
                    dongketnoi();
                }
                return kq;
            }
            

        





        //internal void kn_csdl()
        //{
        //    throw new NotImplementedException();
        //}

        //public SqlConnection kn { get; set; }
    }
}
