﻿namespace Nha_sach_506
{
    partial class frmNhapSach
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnHuyBo_ns = new System.Windows.Forms.Button();
            this.btnNhapSach_ns = new System.Windows.Forms.Button();
            this.btnXoa_ns = new System.Windows.Forms.Button();
            this.btnThem_ns = new System.Windows.Forms.Button();
            this.btnSua_ns = new System.Windows.Forms.Button();
            this.lblNhapSach = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.dgvNhapSach = new System.Windows.Forms.DataGridView();
            this.cboTheLoai_ns = new System.Windows.Forms.ComboBox();
            this.lblTheLoai_ns = new System.Windows.Forms.Label();
            this.lblDonGia_ns = new System.Windows.Forms.Label();
            this.lblNgayNhap_ns = new System.Windows.Forms.Label();
            this.txtDonGia_ns = new System.Windows.Forms.TextBox();
            this.lblSoLuong_ns = new System.Windows.Forms.Label();
            this.txtTenSach_ns = new System.Windows.Forms.TextBox();
            this.lblTenSach_ns = new System.Windows.Forms.Label();
            this.lblNamXuatBan_ns = new System.Windows.Forms.Label();
            this.txtTacGia_ns = new System.Windows.Forms.TextBox();
            this.txtNamXuatBan_ns = new System.Windows.Forms.TextBox();
            this.lblTacGia_ns = new System.Windows.Forms.Label();
            this.lbNgaynhap = new System.Windows.Forms.Label();
            this.lblDate_ns = new System.Windows.Forms.Label();
            this.txtSoLuong_ns = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNhapSach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoLuong_ns)).BeginInit();
            this.SuspendLayout();
            // 
            // btnHuyBo_ns
            // 
            this.btnHuyBo_ns.BackColor = System.Drawing.Color.Tomato;
            this.btnHuyBo_ns.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuyBo_ns.Image = global::Nha_sach_506.Properties.Resources.cancel_dialogue_icone_4250_48;
            this.btnHuyBo_ns.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHuyBo_ns.Location = new System.Drawing.Point(636, 180);
            this.btnHuyBo_ns.Name = "btnHuyBo_ns";
            this.btnHuyBo_ns.Size = new System.Drawing.Size(130, 53);
            this.btnHuyBo_ns.TabIndex = 53;
            this.btnHuyBo_ns.Text = "Hủy bỏ";
            this.btnHuyBo_ns.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHuyBo_ns.UseVisualStyleBackColor = false;
            this.btnHuyBo_ns.Click += new System.EventHandler(this.btnHuyBo_ns_Click);
            // 
            // btnNhapSach_ns
            // 
            this.btnNhapSach_ns.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnNhapSach_ns.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNhapSach_ns.Image = global::Nha_sach_506.Properties.Resources.save_48x48;
            this.btnNhapSach_ns.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNhapSach_ns.Location = new System.Drawing.Point(452, 180);
            this.btnNhapSach_ns.Name = "btnNhapSach_ns";
            this.btnNhapSach_ns.Size = new System.Drawing.Size(150, 53);
            this.btnNhapSach_ns.TabIndex = 52;
            this.btnNhapSach_ns.Text = "Nhập sách";
            this.btnNhapSach_ns.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNhapSach_ns.UseVisualStyleBackColor = false;
            this.btnNhapSach_ns.Click += new System.EventHandler(this.btnNhapSach_ns_Click);
            // 
            // btnXoa_ns
            // 
            this.btnXoa_ns.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnXoa_ns.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa_ns.Image = global::Nha_sach_506.Properties.Resources.cancel_icon;
            this.btnXoa_ns.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnXoa_ns.Location = new System.Drawing.Point(285, 180);
            this.btnXoa_ns.Name = "btnXoa_ns";
            this.btnXoa_ns.Size = new System.Drawing.Size(99, 53);
            this.btnXoa_ns.TabIndex = 51;
            this.btnXoa_ns.Text = "Xóa";
            this.btnXoa_ns.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnXoa_ns.UseVisualStyleBackColor = false;
            this.btnXoa_ns.Click += new System.EventHandler(this.btnXoa_ns_Click);
            // 
            // btnThem_ns
            // 
            this.btnThem_ns.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnThem_ns.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem_ns.Image = global::Nha_sach_506.Properties.Resources.book_add_icon;
            this.btnThem_ns.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnThem_ns.Location = new System.Drawing.Point(29, 180);
            this.btnThem_ns.Name = "btnThem_ns";
            this.btnThem_ns.Size = new System.Drawing.Size(97, 53);
            this.btnThem_ns.TabIndex = 50;
            this.btnThem_ns.Text = "Thêm";
            this.btnThem_ns.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnThem_ns.UseVisualStyleBackColor = false;
            this.btnThem_ns.Click += new System.EventHandler(this.btnThem_ns_Click);
            // 
            // btnSua_ns
            // 
            this.btnSua_ns.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnSua_ns.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSua_ns.Image = global::Nha_sach_506.Properties.Resources.pencil_icon;
            this.btnSua_ns.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSua_ns.Location = new System.Drawing.Point(159, 180);
            this.btnSua_ns.Name = "btnSua_ns";
            this.btnSua_ns.Size = new System.Drawing.Size(93, 53);
            this.btnSua_ns.TabIndex = 49;
            this.btnSua_ns.Text = "Sửa";
            this.btnSua_ns.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSua_ns.UseVisualStyleBackColor = false;
            this.btnSua_ns.Click += new System.EventHandler(this.btnSua_ns_Click);
            // 
            // lblNhapSach
            // 
            this.lblNhapSach.AutoSize = true;
            this.lblNhapSach.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNhapSach.Location = new System.Drawing.Point(271, 6);
            this.lblNhapSach.Name = "lblNhapSach";
            this.lblNhapSach.Size = new System.Drawing.Size(248, 24);
            this.lblNhapSach.TabIndex = 48;
            this.lblNhapSach.Text = "NHẬP THÔNG TIN SÁCH";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.Color.Black;
            this.Label1.Location = new System.Drawing.Point(573, 518);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(221, 15);
            this.Label1.TabIndex = 47;
            this.Label1.Text = "© Copyright 2016. 506 HUTECH Group";
            // 
            // dgvNhapSach
            // 
            this.dgvNhapSach.AllowUserToAddRows = false;
            this.dgvNhapSach.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvNhapSach.BackgroundColor = System.Drawing.Color.Beige;
            this.dgvNhapSach.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNhapSach.Location = new System.Drawing.Point(12, 249);
            this.dgvNhapSach.MultiSelect = false;
            this.dgvNhapSach.Name = "dgvNhapSach";
            this.dgvNhapSach.ReadOnly = true;
            this.dgvNhapSach.RowHeadersVisible = false;
            this.dgvNhapSach.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgvNhapSach.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvNhapSach.Size = new System.Drawing.Size(782, 261);
            this.dgvNhapSach.TabIndex = 46;
            this.dgvNhapSach.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvNhapSach_RowEnter);
            // 
            // cboTheLoai_ns
            // 
            this.cboTheLoai_ns.AutoCompleteCustomSource.AddRange(new string[] {
            "Trinh thám",
            "tinh cảm",
            "thiếu nhi"});
            this.cboTheLoai_ns.FormattingEnabled = true;
            this.cboTheLoai_ns.Items.AddRange(new object[] {
            "Trinh thám",
            "tinh cảm",
            "thiếu nhi"});
            this.cboTheLoai_ns.Location = new System.Drawing.Point(386, 51);
            this.cboTheLoai_ns.Name = "cboTheLoai_ns";
            this.cboTheLoai_ns.Size = new System.Drawing.Size(128, 26);
            this.cboTheLoai_ns.TabIndex = 45;
            this.cboTheLoai_ns.SelectedIndexChanged += new System.EventHandler(this.cboTheLoai_ns_SelectedIndexChanged);
            // 
            // lblTheLoai_ns
            // 
            this.lblTheLoai_ns.AutoSize = true;
            this.lblTheLoai_ns.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTheLoai_ns.Location = new System.Drawing.Point(282, 51);
            this.lblTheLoai_ns.Name = "lblTheLoai_ns";
            this.lblTheLoai_ns.Size = new System.Drawing.Size(67, 17);
            this.lblTheLoai_ns.TabIndex = 44;
            this.lblTheLoai_ns.Text = "Thể loại :";
            // 
            // lblDonGia_ns
            // 
            this.lblDonGia_ns.AutoSize = true;
            this.lblDonGia_ns.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDonGia_ns.Location = new System.Drawing.Point(530, 96);
            this.lblDonGia_ns.Name = "lblDonGia_ns";
            this.lblDonGia_ns.Size = new System.Drawing.Size(68, 17);
            this.lblDonGia_ns.TabIndex = 40;
            this.lblDonGia_ns.Text = "Đơn giá :";
            // 
            // lblNgayNhap_ns
            // 
            this.lblNgayNhap_ns.AutoSize = true;
            this.lblNgayNhap_ns.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayNhap_ns.Location = new System.Drawing.Point(26, 141);
            this.lblNgayNhap_ns.Name = "lblNgayNhap_ns";
            this.lblNgayNhap_ns.Size = new System.Drawing.Size(85, 17);
            this.lblNgayNhap_ns.TabIndex = 43;
            this.lblNgayNhap_ns.Text = "Ngày nhập :";
            // 
            // txtDonGia_ns
            // 
            this.txtDonGia_ns.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGia_ns.Location = new System.Drawing.Point(634, 93);
            this.txtDonGia_ns.Name = "txtDonGia_ns";
            this.txtDonGia_ns.Size = new System.Drawing.Size(128, 25);
            this.txtDonGia_ns.TabIndex = 41;
            this.txtDonGia_ns.TextChanged += new System.EventHandler(this.txtDonGia_ns_TextChanged);
            this.txtDonGia_ns.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDonGia_ns_KeyPress);
            // 
            // lblSoLuong_ns
            // 
            this.lblSoLuong_ns.AutoSize = true;
            this.lblSoLuong_ns.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoLuong_ns.Location = new System.Drawing.Point(530, 54);
            this.lblSoLuong_ns.Name = "lblSoLuong_ns";
            this.lblSoLuong_ns.Size = new System.Drawing.Size(77, 17);
            this.lblSoLuong_ns.TabIndex = 38;
            this.lblSoLuong_ns.Text = "Số lượng :";
            // 
            // txtTenSach_ns
            // 
            this.txtTenSach_ns.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTenSach_ns.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenSach_ns.Location = new System.Drawing.Point(124, 51);
            this.txtTenSach_ns.Name = "txtTenSach_ns";
            this.txtTenSach_ns.Size = new System.Drawing.Size(128, 25);
            this.txtTenSach_ns.TabIndex = 33;
            this.txtTenSach_ns.TextChanged += new System.EventHandler(this.txtTenSach_ns_TextChanged);
            // 
            // lblTenSach_ns
            // 
            this.lblTenSach_ns.AutoSize = true;
            this.lblTenSach_ns.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenSach_ns.Location = new System.Drawing.Point(26, 56);
            this.lblTenSach_ns.Name = "lblTenSach_ns";
            this.lblTenSach_ns.Size = new System.Drawing.Size(77, 17);
            this.lblTenSach_ns.TabIndex = 32;
            this.lblTenSach_ns.Text = "Tên sách :";
            // 
            // lblNamXuatBan_ns
            // 
            this.lblNamXuatBan_ns.AutoSize = true;
            this.lblNamXuatBan_ns.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNamXuatBan_ns.Location = new System.Drawing.Point(272, 99);
            this.lblNamXuatBan_ns.Name = "lblNamXuatBan_ns";
            this.lblNamXuatBan_ns.Size = new System.Drawing.Size(106, 17);
            this.lblNamXuatBan_ns.TabIndex = 36;
            this.lblNamXuatBan_ns.Text = "Năm xuất bản :";
            // 
            // txtTacGia_ns
            // 
            this.txtTacGia_ns.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTacGia_ns.Location = new System.Drawing.Point(124, 99);
            this.txtTacGia_ns.Name = "txtTacGia_ns";
            this.txtTacGia_ns.Size = new System.Drawing.Size(128, 25);
            this.txtTacGia_ns.TabIndex = 35;
            this.txtTacGia_ns.TextChanged += new System.EventHandler(this.txtTacGia_ns_TextChanged);
            // 
            // txtNamXuatBan_ns
            // 
            this.txtNamXuatBan_ns.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamXuatBan_ns.Location = new System.Drawing.Point(384, 96);
            this.txtNamXuatBan_ns.Name = "txtNamXuatBan_ns";
            this.txtNamXuatBan_ns.Size = new System.Drawing.Size(128, 25);
            this.txtNamXuatBan_ns.TabIndex = 37;
            this.txtNamXuatBan_ns.TextChanged += new System.EventHandler(this.txtNamXuatBan_ns_TextChanged);
            // 
            // lblTacGia_ns
            // 
            this.lblTacGia_ns.AutoSize = true;
            this.lblTacGia_ns.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTacGia_ns.Location = new System.Drawing.Point(26, 102);
            this.lblTacGia_ns.Name = "lblTacGia_ns";
            this.lblTacGia_ns.Size = new System.Drawing.Size(64, 17);
            this.lblTacGia_ns.TabIndex = 34;
            this.lblTacGia_ns.Text = "Tác giả :";
            // 
            // lbNgaynhap
            // 
            this.lbNgaynhap.AutoSize = true;
            this.lbNgaynhap.Location = new System.Drawing.Point(131, 140);
            this.lbNgaynhap.Name = "lbNgaynhap";
            this.lbNgaynhap.Size = new System.Drawing.Size(0, 18);
            this.lbNgaynhap.TabIndex = 54;
            this.lbNgaynhap.Click += new System.EventHandler(this.label2_Click);
            // 
            // lblDate_ns
            // 
            this.lblDate_ns.AutoSize = true;
            this.lblDate_ns.Location = new System.Drawing.Point(121, 140);
            this.lblDate_ns.Name = "lblDate_ns";
            this.lblDate_ns.Size = new System.Drawing.Size(50, 18);
            this.lblDate_ns.TabIndex = 55;
            this.lblDate_ns.Text = "label2";
            // 
            // txtSoLuong_ns
            // 
            this.txtSoLuong_ns.Location = new System.Drawing.Point(634, 48);
            this.txtSoLuong_ns.Name = "txtSoLuong_ns";
            this.txtSoLuong_ns.Size = new System.Drawing.Size(128, 26);
            this.txtSoLuong_ns.TabIndex = 56;
            // 
            // frmNhapSach
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(805, 535);
            this.Controls.Add(this.txtSoLuong_ns);
            this.Controls.Add(this.lblDate_ns);
            this.Controls.Add(this.lbNgaynhap);
            this.Controls.Add(this.btnHuyBo_ns);
            this.Controls.Add(this.btnNhapSach_ns);
            this.Controls.Add(this.btnXoa_ns);
            this.Controls.Add(this.btnThem_ns);
            this.Controls.Add(this.btnSua_ns);
            this.Controls.Add(this.lblNhapSach);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.dgvNhapSach);
            this.Controls.Add(this.cboTheLoai_ns);
            this.Controls.Add(this.lblTheLoai_ns);
            this.Controls.Add(this.lblDonGia_ns);
            this.Controls.Add(this.lblNgayNhap_ns);
            this.Controls.Add(this.txtDonGia_ns);
            this.Controls.Add(this.lblSoLuong_ns);
            this.Controls.Add(this.txtTenSach_ns);
            this.Controls.Add(this.lblTenSach_ns);
            this.Controls.Add(this.lblNamXuatBan_ns);
            this.Controls.Add(this.txtTacGia_ns);
            this.Controls.Add(this.txtNamXuatBan_ns);
            this.Controls.Add(this.lblTacGia_ns);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmNhapSach";
            this.Text = "Nhập thông tin sách";
            this.Load += new System.EventHandler(this.frmNhapSach_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvNhapSach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoLuong_ns)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button btnHuyBo_ns;
        internal System.Windows.Forms.Button btnNhapSach_ns;
        internal System.Windows.Forms.Button btnXoa_ns;
        internal System.Windows.Forms.Button btnThem_ns;
        internal System.Windows.Forms.Button btnSua_ns;
        internal System.Windows.Forms.Label lblNhapSach;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.DataGridView dgvNhapSach;
        internal System.Windows.Forms.ComboBox cboTheLoai_ns;
        internal System.Windows.Forms.Label lblTheLoai_ns;
        internal System.Windows.Forms.Label lblDonGia_ns;
        internal System.Windows.Forms.Label lblNgayNhap_ns;
        internal System.Windows.Forms.TextBox txtDonGia_ns;
        internal System.Windows.Forms.Label lblSoLuong_ns;
        internal System.Windows.Forms.TextBox txtTenSach_ns;
        internal System.Windows.Forms.Label lblTenSach_ns;
        internal System.Windows.Forms.Label lblNamXuatBan_ns;
        internal System.Windows.Forms.TextBox txtTacGia_ns;
        internal System.Windows.Forms.TextBox txtNamXuatBan_ns;
        internal System.Windows.Forms.Label lblTacGia_ns;
        private System.Windows.Forms.Label lbNgaynhap;
        private System.Windows.Forms.Label lblDate_ns;
        private System.Windows.Forms.NumericUpDown txtSoLuong_ns;
    }
}